# ASCII Art GUI
# Daniel Simpson
# 4/1/23
# A script to call linux ascii art utilities from a gui to better learn tkinter

import tkinter as tk
import os

# root of window
root = tk.Tk()

title = tk.Label(text="ASCII Art GUI")
title.pack()

# need button to submit text
# need to combine string with bash and submit to figlet

# button function: assign text to var, clear textbox, submit text to figlet
def submit():
    #global userInput 
    userInput = textbox.get()
    textbox.delete(0, tk.END) # clear text box
    cmd = "figlet " + userInput # works, but need to add logic for escape sequences 
    os.system(cmd)

# textbox
textbox = tk.Entry(fg="green", bg="black", width=20)
textbox.pack()
#userInput = ""

submitButton = tk.Button(text="submit", bg="red", fg="white", command=submit)
submitButton.pack()

# loop the interface
root.mainloop()
